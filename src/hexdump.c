#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

int curgroup = 0; // To know where 8 group 
int dumpgroup = 0; // To know where newline should be
int hexnum = 1; // To put space after every second hex number
bool canonical = true; // -C flag

int chardump(char *data, int start_of_line, int end_of_line /* to display only 16 chars */) {
	printf("|");
	for (int i=start_of_line; i < end_of_line+1; i++) {
		if (isprint(data[i]) == 0 || data[i] == '\n' || data[i] == '\0')
			printf(".");
		else 
			printf("%c", data[i]);
	}
	printf("|\n");
	return 0;
}


int hexdump(char *data) {
	//printf("%s\n", data); // FIXME
	int cur_start_of_line = 0;
	for (int i=0; i < strlen(data); i++) {
		if (data[i] == '\0') 
			printf("00");
		else {
			if (!canonical)
				printf("%02x", data[i]);
			else {
				printf("%02x ", data[i]);
			}
		}

		hexnum++;
		if (hexnum%2 && hexnum >= 2) {
			if (!canonical) 
				printf(" ");
			curgroup++;
		}
		if (curgroup == 4) {
			if (canonical) {
				printf("  ");
			}
			curgroup = 0;
			dumpgroup++;
		}
		if (dumpgroup == 2) {
			dumpgroup = 0;
			chardump(data, cur_start_of_line, i);
			hexnum = 0;
			cur_start_of_line = i;
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc < 2) {
		printf("%s: not enought arguments\n", argv[0]);
		return 1;
	}
	FILE *infile = fopen(argv[1], "r");
	char data[10000]; // FIXME
	char ch;
	int byte = 0; // FIXME
	if (infile == NULL) {
		printf("Cannot find file: \n");
		return 1;
	}
	while ((ch = fgetc(infile)) != EOF)
        {
            data[byte] = ch;
	    byte++;
        }
	hexdump(data);
	printf("\n"); // FIXME
	fclose(infile);
}
